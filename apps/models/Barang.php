<?php

class Barang
{

    private $id;
    private $nama;
    private $jumlah;

    public function __construct()
    {
        $this->id = "1";
        $this->nama = "Buku";
        $this->jumlah = "15";
    }

    public function getData()
    {
        return "Data dari model barang : $this->id, $this->id, $this->jumlah ";
    }

    public function getDataOne()
    {
        $data = [
            'id'    => $this->id,
            'nama'    => $this->nama,
            'qty'    => $this->jumlah,
        ];
        return $data;
    }
}
